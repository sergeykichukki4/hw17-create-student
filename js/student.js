"use strict";

function checkStringCorrectness(message) {
    let txtString;

    do {
        txtString = prompt(message)
    } while (txtString === null || txtString.trim() === "")

    return txtString;
}

function checkMarkInput(message) {
    let mark;

    do {
        mark = prompt(message);
    } while (!mark || isNaN(mark) || !Number.isInteger(+mark) || +mark < 1 || +mark > 10)

    return +mark;
}


const student = {
    _name: null,
    _surname: null,

    tabel: {
        addGradedSubject() {
            while (true) {
                let subject = prompt("Input subject name: ");
                if (subject === null || subject.trim() === "") break;
                subject = subject.toLowerCase();
                this[subject] = checkMarkInput("Enter your mark (from 1 to 10): ");
            }
        },

        countBadMarks() {
            let numOfBadMarks = 0;
            let counter = 0;
            for (const key in this) {
                if (typeof this[key] === "number") {
                    if (this[key] < 4) numOfBadMarks++;
                    counter++;
                }
            }
            if (numOfBadMarks > 0) {
                return `${numOfBadMarks} of ${counter} are bad marks that have to be improved`
            } else {
                return `Student passed and transferred to next course`;
            }
        },

        averageMark() {
            let counter = 0;
            let sum = 0;
            for (const key in this) {
                if (typeof this[key] === "number") {
                    sum += +this[key];
                    counter++;
                }
            }
            return sum / counter;
        },

        hasScholarship() {
            if (this.averageMark() > 7) {
                return `Student got scholarship`;
            } else {
                return `Student has not hot scholarship. Try harder next time!`;
            }
        },
    },

    setName() {
        this._name = checkStringCorrectness("Enter name: ");
    },

    setSurname() {
        this._surname = checkStringCorrectness("Enter surname: ");
    },

    set name(value) {
        this._name = value;
    },

    get name() {
        return this._name;
    },

    set surname(value) {
        this._surname = value;
    },

    get surname() {
        return this._surname;
    },
}


student.setName();
student.setSurname();
student.tabel.addGradedSubject();
console.log(student.tabel.averageMark());
console.log(student.tabel.countBadMarks());
console.log(student.tabel.hasScholarship());
console.log(student);


